Data-Science.pl Blog
====================

Configuration
---
- Install the requried gems: `bundle install`
- Run the jekyll server: `bundle exec jekyll serve`. You should have a server up and running locally at <http://localhost:4000>.
- If you want to enable live reload: `bundle exec guard`.
- If you want to recompile rmarkdown automaticly: `./watchRPost.sh ./_posts_knitr/Post.Rmdf`

The main settings happen in side of the _config.yml file:

### Preview and publish
For both those actions we use rake:
- To preview type `rake` or `rake preview` and visit http://preview.data-science.pl (it's hosted via gh-pages in this repo: https://github.com/Appsilon/preview-datascience-blog)
- To publish type `rake publish` and visit http://data-science.pl (it's hosted via gh-pages in this repo: https://github.com/Appsilon/datascience-blog)

### Site

Main settings for the site

* **title**: name of your site
* **description**: description of your site
* **logo**: small logo for the site (300x * 300x)
* **cover**: large background image on the index page

* **name**: name site owner
* **email**: mail address of the site owner
* **author**: author name
* **author_image**: small image of author (300x * 300px)
* **disqus**: add a disqus forum for your post

### Social

The template allows to add all major social plattforms to your site.
Fill the the form for each plattform. If you leave the share_* entries empty, the sharing buttons below a post are not shown.  If you leave the **url** and **desc** empty the icons are not shown on the index page, but the share icons on the article pages remains untouched (Thanks to [Phil](https://github.com/philsturgeon))

* **icon**:	name of social plattform (must match a name of [font-awsome](http://fortawesome.github.io/Font-Awesome/) icon set )
* **url**:	url of your account
* **desc**: slogan of the plattform
* **share_url**: share url
* **share_title**: first part of url for the title
* **share_link**: second part of the share url for the link to the post

The Liquid template engine will magical combine the different parts to a share url.

```
http://twitter.com/share?text=post_title&amp;url=post_url
````

See [_config.yml](https://github.com/dirkfabisch/mediator/blob/master/_config.yml) for more examples.

Licensing
---------

[MIT](https://github.com/dirkfabisch/mediator/blob/master/LICENCE) with no added caveats, so feel free to use this on your site without linking back to me or using a disclaimer or anything silly like that.

Contact
-------
I'd love to hear from you at [@dirkfabisch](https://twitter.com/dirkfabisch). Feel free to open issues if you run into trouble or have suggestions. Pull Requests always welcome.
